;; test rule one 

(defun init-rule (&optional reverse)
  (init)
  (new-type 'id :constants '(i1 i2 i3 i4 i5 i6 i7))
  
  (predicate 'next :world 'closed :arguments '((id id) (value id)))

  (setq post-d '((ppfn 'next nil 'array)))
  
  (conditional 'trans
               :conditions '(
                             (next (id (a)) (value (b)))
                             (next (id (b)) (value (c)))
                             )
               :actions '((next (id (a)) (value (c))))
               )

  (when reverse
    (conditional 'reverse
                 :conditions '(
                               (next (id (a)) (value (b)))
                               )
                 :actions '((next (id (b)) (value (a))))
                 )
    )
  t)

  (evidence '((next (id i1) (value i2))
              (next (id i2) (value i3))
              ))
  t)

(d 1)
(ppfn 'next nil 'array info-stream)